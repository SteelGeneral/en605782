/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tripsplit.database;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;

import junit.framework.TestCase;
import tripsplit.api.Expense;

/**
 *
 * @author brian
 */
public class DBConnectionTest extends TestCase {
    
    public DBConnectionTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        DBConnection.closeConnection();
    }

    /**
     * Test of getDao method, of class DBConnection.
     * @throws java.lang.Exception
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void testGetDao() throws Exception {
        System.out.println("getDao");
        
        // expense
        String className = "Expense";
        Dao expResult = DaoManager.createDao(DBConnection.getConnection(), Expense.class);
        Dao result = DBConnection.getDao(className);        
        
        assertEquals(expResult.countOf(), result.countOf());
    }
    
}
