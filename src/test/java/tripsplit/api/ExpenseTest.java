/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tripsplit.api;

import java.sql.SQLException;
import junit.framework.TestCase;
import org.junit.Test;

/**
 *
 * @author brian
 */
public class ExpenseTest extends TestCase {

    public ExpenseTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testPass() throws SQLException {
        assertTrue(true);
    }

    /*
     @Test
     public void testCreateExpense() throws SQLException {
     System.out.println("testCreateExpense");
     Expense instance = new Expense();
     instance.setTitle("Testing testing - testCreateExpense");
     instance.saveObj();

     // load from database
     Expense newExpense = Expense.getExpense(instance.getExpenseID());

     assertEquals(instance.getTitle(), newExpense.getTitle());
     }

     @Test
     public void testAddTrip() throws SQLException {
     System.out.println("testAddTrip");
        
     // objects
     Expense testExpense = new Expense();
     Trip testTrip = TestHelper.getRandomTrip();
        
     // modify expense
     testExpense.setTitle("Testing");
     testExpense.setTrip(testTrip);
        
     //save
     testExpense.saveObj();
        
     //load new expense
     Expense loadExpense = Expense.getExpense(testExpense.getExpenseID());
        
     // checks for expense
     assertEquals(testExpense.getTitle(), loadExpense.getTitle());
     assertEquals(testExpense.getTrip().getTripID(), loadExpense.getTrip().getTripID());
        
     //checks for the trip
     testTrip.refreshObj();
     assertEquals(testExpense.getExpenseID(), ((Expense) (testTrip.getExpenses().toArray()[0])).getExpenseID());
     }
    
     @Test
     public void testAddUser() throws SQLException {
     System.out.println("testAddUser");
        
     // objects
     Expense testExpense = new Expense();
     User testUser1 = TestHelper.getRandomUser();
     User testUser2 = TestHelper.getRandomUser();
        
     //save
     testExpense.saveObj();
        
     // modify expense
     testExpense.setTitle("Testing");
     testExpense.setCollector(testUser1);
     testExpense.addDebtor(testUser2);
        
     //save
     testExpense.saveObj();
        
     //load new expense
     Expense loadExpense = Expense.getExpense(testExpense.getExpenseID());
        
     // checks for expense
     assertEquals(testExpense.getTitle(), loadExpense.getTitle());
     assertEquals(testUser1.getUserID(), testExpense.getCollector().getUserID());
     assertEquals(testUser2.getUserID(), testExpense.getDebtors().get(0).getUserID());
        
     //checks for the user
     //load new expense
     User loadUser = User.getUser(testUser1.getUserID());
        
     assertEquals(testExpense.getExpenseID(), ((Expense) (testUser1.getExpenses().toArray()[0])).getExpenseID());
     assertEquals(testExpense.getExpenseID(), ((Expense) (testUser2.getExpenses().toArray()[0])).getExpenseID());
     }
     */
}
