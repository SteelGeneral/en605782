package tripsplit.userAccess;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tripsplit.api.User;

/**
 * @author Seth
 *
 *         The RegisterUser Servlet takes parameters from register.jsp and
 *         builds a currentUser session attribute, forwards user to
 *         mytrips.jsp, and persists new registration in db.
 */
@WebServlet(urlPatterns = { "/register", "/Register" })
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegistrationServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		User currentUser = new User();
		currentUser.setUserName(username);
		currentUser.setPassword(password);

		try {
			currentUser.saveObj();
		} catch (SQLException e) {
			request.getRequestDispatcher(
					"/register.jsp?message=registrationFailed").forward(
					request, response);
		}

		request.getSession().setAttribute("currentUser", currentUser);
		response.sendRedirect("/TripSplit/app/mytrips.jsp");
	}

}
