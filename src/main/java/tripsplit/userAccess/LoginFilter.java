package tripsplit.userAccess;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Seth
 * 
 *         The Login Filter checks login status. Ignores special resources like
 *         css folder and login.jsp. If request contains parameters to
 *         instantiate a session, sends to login servlet. If request does not
 *         contain currentUser attribute, filter sends them to login.jsp because
 *         no valid session is active. Otherwise, there is a valid session and
 *         filter sends user to desired page.
 */
@WebFilter(urlPatterns = { "/app/*" })
public class LoginFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public LoginFilter() {

	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {

	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;

		if (req.getSession().getAttribute("currentUser") == null) {
			// Invalid session, not logged in
			// Send to login.jsp if not already navigating there
			HttpServletResponse res = (HttpServletResponse) response;
			res.sendRedirect("/TripSplit/login.jsp");
		} else {
			// valid session, continue as requested
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {

	}
}