/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tripsplit.database;

/**
 *
 * @author brian
 */
public class DBConnectionInformation {

    // referencing shared MYSQL database
    protected final static String DATABASE_URL = "jdbc:mysql://db4free.net:3306/tripsplitdb";
    protected final static String DATABASE_USERNAME = "tripsplituser";
    protected final static String DATABASE_PASSWORD = "tripsplitpass";

    // Brian local machine
    //protected final static String DATABASE_URL = "jdbc:mysql://localhost:3306/tripsplitdb";
    //protected final static String DATABASE_USERNAME = "root";
    //protected final static String DATABASE_PASSWORD = "root";
    
    protected final static boolean buildDatabase = false;
}