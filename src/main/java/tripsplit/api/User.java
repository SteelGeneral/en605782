package tripsplit.api;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Steven The User class stores all data about the TripSplit User.
 */
@DatabaseTable(tableName = "TS_User")
public class User extends DaoCommon implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="static variables">
    // columns
    public final static String COLUMN_USER_ID = "userID";
    private static final String COLUMN_USERNAME = "userName";

    // Logger
    private static final Logger logger = Logger.getLogger(User.class.getName());

    private static final long serialVersionUID = -6024679733398892494L;

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Variables and database fields">
    @DatabaseField(generatedId = true, columnName = COLUMN_USER_ID)
    private int userID;

    @DatabaseField(uniqueIndex = true, canBeNull = false, columnName = COLUMN_USERNAME)
    private String userName;

    @DatabaseField(canBeNull = false)
    private String password;

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="constructor">
    public User() {}

    // </editor-fold>
    /**
     * Uses the database to fetch a user
     *
     * @param userName
     * @return
     */
    @SuppressWarnings("unchecked")
	public static User getUser(String userName) {
       
    	if(userName == null || userName == "") return null;
    	
    	// convert to lowercase
        userName = userName.toLowerCase();

        // temp user
        User tempUser = new User();

        // access database
        try {
            // get the first user
            List<User> tempUserList = tempUser.getDao().queryForEq(COLUMN_USERNAME, userName);
            if (null != tempUserList && 0 < tempUserList.size()) {
                tempUser = (User) tempUserList.get(0);
                return tempUser;
            } else {
                return null;
            }
        } catch (SQLException | NullPointerException ex) {
            logger.log(Level.SEVERE, "Could not retrieve the user for username: " + userName, ex);
            return null;
        }
    }

    /**
     * Uses database to fetch a user given the userID
     *
     * @param userID
     * @return
     */
    @SuppressWarnings("unchecked")
	public static User getUser(int userID) {
        // temp user
        User temp = new User();

        // access database
        try {
            // get the first user
            Integer idInt = userID;
            temp = (User) temp.getDao().queryForId(idInt);

            // got it, return
            return temp;
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Could not retrieve the user for userID: " + userID, ex);
            return null;
        }
    }

    /**
     * Uses the database to fetch all the users
     *
     * @return
     * @throws SQLException
     */
    @SuppressWarnings("unchecked")
	public List<User> getAllUsers() throws SQLException {
        User tempUser = new User();

        // database query
        List<User> listUser = tempUser.getDao().queryBuilder().query();

        return listUser;
    }

    /**
     * Retrieves the users that are associated with this trip
     *
     * source =
     * https://github.com/j256/ormlite-jdbc/blob/master/src/test/java/com/j256/ormlite/examples/manytomany/ManyToManyMain.java
     *
     * @return
     * @throws SQLException
     */
    @SuppressWarnings("unchecked")
	public List<Trip> getTrips() throws SQLException {
        UserTrip tempUserTrip = new UserTrip();

        // database query
        List<UserTrip> listUserTrip = tempUserTrip.getDao().queryForEq(UserTrip.COLUMN_USER_ID, getUserID());

        // convert to a list of users
        List<Trip> foundTrips = new ArrayList<>(listUserTrip.size());
        for (UserTrip curUserTrip : listUserTrip) {
            Trip curTrip = curUserTrip.getTrip();
            curTrip.refreshObj();
            foundTrips.add(curTrip);
        }
        return foundTrips;
    }

    /**
     * Adds a trip by creating a new joiner for user and trip. The temp join is
     * saved to the database.
     *
     * @param newTrip
     * @throws SQLException
     */
    public void addTrip(Trip newTrip) throws SQLException {
        UserTrip tempUserTrip = new UserTrip(this, newTrip);
        tempUserTrip.saveObj();
    }

    @SuppressWarnings("unchecked")
	public List<Expense> getExpenses() throws SQLException {
        UserExpense tempUserExpense = new UserExpense();

        // database query
        List<UserExpense> listUserExpense = tempUserExpense.getDao().queryForEq(UserExpense.COLUMN_USER_ID, getUserID());

        // convert to a list of users
        List<Expense> foundExpenses = new ArrayList<>(listUserExpense.size());
        for (UserExpense curUserExpense : listUserExpense) {
            Expense curExpense = curUserExpense.getExpense();
            curExpense.refreshObj();
            foundExpenses.add(curExpense);
        }
        return foundExpenses;
    }

    @SuppressWarnings("unchecked")
	public List<Expense> getCollectorExpenses() throws SQLException {
        UserExpense tempUserExpense = new UserExpense();

        // database query
        List<UserExpense> listUserExpense = tempUserExpense.getDao().queryForEq(UserExpense.COLUMN_USER_ID, getUserID());

        // convert to a list of users
        List<Expense> foundExpenses = new ArrayList<>(listUserExpense.size());
        for (UserExpense curUserExpense : listUserExpense) {
            // filter
            if (true == curUserExpense.isCollector()) {
                Expense curExpense = curUserExpense.getExpense();
                curExpense.refreshObj();
                foundExpenses.add(curExpense);
            }
        }
        return foundExpenses;
    }

    @SuppressWarnings("unchecked")
	public List<Expense> getDebtorExpenses() throws SQLException {
        UserExpense tempUserExpense = new UserExpense();

        // database query
        List<UserExpense> listUserExpense = tempUserExpense.getDao().queryForEq(UserExpense.COLUMN_USER_ID, getUserID());

        // convert to a list of users
        List<Expense> foundExpenses = new ArrayList<>(listUserExpense.size());
        for (UserExpense curUserExpense : listUserExpense) {
            // filter
            if (false == curUserExpense.isCollector()) {
                Expense curExpense = curUserExpense.getExpense();
                curExpense.refreshObj();
                foundExpenses.add(curExpense);
            }
        }
        return foundExpenses;
    }

    // <editor-fold defaultstate="collapsed" desc="interface methods">
    @Override
    public String getClassName() {
        return User.class.getSimpleName();
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Get and Set for variables">
    public int getUserID() {
        return userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Override base methods">
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.userID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.userID != other.userID) {
            return false;
        }
        return true;
    }

    // </editor-fold>
}
