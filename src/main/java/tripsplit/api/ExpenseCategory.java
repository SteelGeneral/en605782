package tripsplit.api;

/**
 * @author Steven
 * The ExpenseCategory contains all possible categories an expense could fall within.
 */
public enum ExpenseCategory {
	GAS,TOLLS,LODGING,FOOD,DRINKS,SOUVENIRS,EVENT_COSTS,REPAYMENT,MISC
}
