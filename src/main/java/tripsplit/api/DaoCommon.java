/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tripsplit.api;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import tripsplit.database.DBConnection;

/**
 * Contains methods for all the DAOs to make coding easier
 * @author brian
 */
public abstract class DaoCommon {

    public abstract String getClassName();

    /**
     * Saves or updates the current object to the database
     * @throws SQLException 
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void saveObj() throws SQLException {
        Dao curDao = DBConnection.getDao(this.getClassName());
        curDao.createOrUpdate(this);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void deleteObj(int id) throws SQLException{
        Dao curDao = DBConnection.getDao(this.getClassName());
        curDao.deleteById(id);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void refreshObj() throws SQLException {        
        Dao curDao = DBConnection.getDao(this.getClassName());
        curDao.refresh(this);
    }
    
    /**
     * Returns the DAO for the class
     * @return 
     */
    @SuppressWarnings("rawtypes")
	public Dao getDao() {
        return DBConnection.getDao(this.getClassName());        
    }
    
    public long rowsInDatabase()
    {
        try {
            return getDao().countOf();
        } catch (SQLException ex) {
            Logger.getLogger(DaoCommon.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

}
