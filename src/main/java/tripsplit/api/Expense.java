package tripsplit.api;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Steven The Expense class stores all expenses associated with a Trip.
 */
@DatabaseTable(tableName = "TS_Expense")
public class Expense extends DaoCommon implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="static variables">
    // Logger
    private static final Logger logger = Logger.getLogger(Expense.class.getName());

    private static final long serialVersionUID = 1665007978752406703L;
    public static final String COLUMN_PAYEE = "payee_id";
    public static final String COLUMN_PAYER = "payer_id";
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Variables and database fields">
    @DatabaseField(generatedId = true)
    private int expenseID;

    @DatabaseField
    private String title;

    @DatabaseField
    private Date date;

    @DatabaseField
    private float cost;

    @DatabaseField
    private ExpenseCategory category;

    @DatabaseField
    private String notes;

    @DatabaseField(foreign = true)
    private Trip trip;

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="constructor">
    public Expense() {
        super();
    }
    // </editor-fold>

    /**
     * Accesses the database and returns the expense given the expenseID
     *
     * @param expenseID
     * @return
     */
    @SuppressWarnings("unchecked")
	public static Expense getExpense(int expenseID) {

        // temp expense
        Expense tempExpense = new Expense();

        // access database
        try {
            // get the first expense
            Integer expenseIdInteger = expenseID;
            tempExpense = (Expense) tempExpense.getDao().queryForId(expenseIdInteger);

            // got it, return
            return tempExpense;
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Could not retrieve the expense for expenseID: " + expenseID, ex);
            return null;
        }
    }

    public void setCollector(User user) throws SQLException {
        UserExpense tempUserExpense = new UserExpense(user, this, true);
        tempUserExpense.saveObj();
    }

    public void addDebtor(User user) throws SQLException {
        UserExpense tempUserExpense = new UserExpense(user, this, false);
        tempUserExpense.saveObj();
    }

    @SuppressWarnings("unchecked")
	public User getCollector() throws SQLException {
        UserExpense tempUserExpense = new UserExpense();

        // database query
        List<UserExpense> listUserExpense = tempUserExpense.getDao().queryForEq(UserExpense.COLUMN_EXPENSE_ID, getExpenseID());

        // remove the debtors
        Iterator<UserExpense> a = listUserExpense.iterator();
        while (a.hasNext()) {
            if (a.next().isCollector() == false) {
                a.remove();
            }
        }

        // convert to a list of users
        List<User> foundUsers = new ArrayList<>(listUserExpense.size());
        for (UserExpense curUserExpense : listUserExpense) {
            User curUser = curUserExpense.getUser();
            curUser.refreshObj();
            foundUsers.add(curUser);
        }
        // return first user or empty name
        if (0 == foundUsers.size()) {
            User newUser = new User();
            newUser.setUserName(" ");
            newUser.setPassword(" ");
            newUser.saveObj();
            this.setCollector(newUser);
            this.saveObj();
            return newUser;
        } else {
            return foundUsers.get(0);
        }
    }
        
        
    public void removeExpense() throws SQLException {
        this.deleteObj(this.getExpenseID());
    }

    @SuppressWarnings("unchecked")
	public List<User> getDebtors() throws SQLException {
        UserExpense tempUserExpense = new UserExpense();

        // database query
        List<UserExpense> listUserExpense = tempUserExpense.getDao().queryForEq(UserExpense.COLUMN_EXPENSE_ID, getExpenseID());

        // remove the debtors
        Iterator<UserExpense> a = listUserExpense.iterator();
        while (a.hasNext()) {
            if (a.next().isCollector() == true) {
                a.remove();
            }
        }

        // convert to a list of users
        List<User> foundUsers = new ArrayList<>(listUserExpense.size());
        for (UserExpense curUserExpense : listUserExpense) {
            User curUser = curUserExpense.getUser();
            curUser.refreshObj();
            foundUsers.add(curUser);
        }
        // return first user
        return foundUsers;
    }

    // <editor-fold defaultstate="collapsed" desc="Get and Set for variables">
    public int getExpenseID() {
        return expenseID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public ExpenseCategory getCategory() {
        return category;
    }

    public void setCategory(ExpenseCategory category) {
        this.category = category;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="interface methods">
    @Override
    public String getClassName() {
        return Expense.class.getSimpleName();
    }
    // </editor-fold>

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + this.expenseID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Expense other = (Expense) obj;
        if (this.expenseID != other.expenseID) {
            return false;
        }
        return true;
    }

}
