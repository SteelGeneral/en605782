package tripsplit.api;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kelly
 */
public class TripSplitController extends HttpServlet {

    private static final long serialVersionUID = 8960457536953685499L;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        Trip trip = new Trip();
        if (session.getAttribute("trip") != null) {
            trip = (Trip) session.getAttribute("trip");
        }

        User currentUser = new User();
        if (session.getAttribute("currentUser") != null) {
            currentUser = (User) session.getAttribute("currentUser");
        }

        String createTrip = request.getParameter("newTrip");
        if (createTrip != null) {
            Trip newTrip;
            try {
                newTrip = createTrip(request, currentUser);
                addUsersToTrip(request, currentUser, newTrip);
                 session.setAttribute("currentTrips", currentUser.getTrips());
                 response.sendRedirect("/TripSplit/app/trip.jsp?currentTripID="+newTrip.getTripID());
                return;
            } catch (Exception ex) {
                Logger.getLogger(TripSplitController.class.getName()).log(Level.SEVERE, null, ex);
                forward("/app/error.jsp?currentTripID="+trip.getTripID()+"&error="+ex.getMessage(),request,response);
            }
                      
        }

        String addExpense = request.getParameter("addExpense");
        if (addExpense != null) {
            Expense newExpense = createExpense(request, response, trip);
            try {
                addUsersToExpense(request, newExpense);
            } catch (SQLException ex) {
                Logger.getLogger(TripSplitController.class.getName()).log(Level.SEVERE, null, ex);
                forward("/app/error.jsp?currentTripID="+trip.getTripID()+"&error="+ex.getMessage(),request,response);
            }
            forward("/app/trip.jsp?currentTripID=" + trip.getTripID(), request, response);
            return;
        }

        String removeMember = request.getParameter("removeMember");
        if (removeMember != null) {
            try {
                trip.removeUser(removeMember);
            } catch (SQLException ex) {
                request.setAttribute("trip", trip);
                Logger.getLogger(TripSplitController.class.getName()).log(Level.SEVERE, null, ex);
                forward("/app/error.jsp?currentTripID="+trip.getTripID()+"&error="+ex.getMessage(),request,response);
            }
            request.setAttribute("trip", trip);
            forward("/app/trip.jsp?currentTripID=" + trip.getTripID(), request, response);
            return;
        }

        //process add member submit button
        String addMembers = request.getParameter("addMembers");
        if (addMembers != null) {
            try {
                addUsersToTrip(request, currentUser, trip);
            } catch (SQLException ex) {
                Logger.getLogger(TripSplitController.class.getName()).log(Level.SEVERE, null, ex);
                forward("/app/error.jsp?currentTripID="+trip.getTripID()+"&error="+ex.getMessage(),request,response);
            }
            forward("/app/trip.jsp?currentTripID=" + trip.getTripID(), request, response);
            return;
        }

        // process remove expense
        String removeExpenseStrID = request.getParameter("removeExpense");
        if (removeExpenseStrID != null) {
            try {
                removeExpense(request, removeExpenseStrID);
            } catch (SQLException ex) {
                Logger.getLogger(TripSplitController.class.getName()).log(Level.SEVERE, null, ex);
                forward("/app/error.jsp?currentTripID="+trip.getTripID()+"&error="+ex.getMessage(),request,response);
            }
            forward("/app/trip.jsp?currentTripID=" + trip.getTripID(), request, response);
            return;
        }
    }

    private void removeExpense(HttpServletRequest request, String expenseID) throws SQLException {
        int expID = Integer.parseInt(expenseID);
        Expense tempExpense = Expense.getExpense(expID);
        tempExpense.removeExpense();
    }

    private Trip createTrip(HttpServletRequest request, User currentUser) throws SQLException {
        Trip newTrip = new Trip();
        newTrip.setTripName(request.getParameter("tripName"));
        newTrip.setComments(request.getParameter("comments"));
        newTrip.addUser(currentUser);
        newTrip.saveObj();
        return newTrip;
    }

    private void addUsersToTrip(HttpServletRequest request, User curUser, Trip curTrip) throws SQLException {
        String[] memberNames = request.getParameterValues("members");

        // return if there are no members
        if (null == memberNames) {
            return;
        }

        List<String> newMemberNames = new ArrayList<String>(Arrays.asList(memberNames)); // convert to ArrayList<String> 
        
        // We can add the current user to a trip if they are not already part of the trip
        if (!newMemberNames.contains(curUser.getUserName())) {
            newMemberNames.add(curUser.getUserName());
        }
        // add new users 
        for (String currentMemberName : newMemberNames) {

            // get the user to add
            User currentMember = User.getUser(currentMemberName);

            // continue if not null
            if (null != currentMemberName) {

                // continue if the combination does not exist already
                UserTrip userTrip = new UserTrip();
                userTrip = userTrip.getUserTrip(currentMember.getUserID(), curTrip.getTripID()); // get combination

                // continue if the combination does not exist
                if (null == userTrip) {
                    userTrip = new UserTrip();
                    userTrip.setTrip(curTrip);
                    userTrip.setUser(currentMember);
                    userTrip.saveObj();
                }
            }
        }
    }

    private Expense createExpense(HttpServletRequest request, HttpServletResponse response, Trip trip) {
        Expense newExpense = new Expense();
        newExpense.setTrip(trip);
        newExpense.setTitle(request.getParameter("expenseTitle"));
        newExpense.setCost(Float.parseFloat(request.getParameter("expenseCost")));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date expenseDate = dateFormat.parse(request.getParameter("expenseDate"));
            newExpense.setDate(expenseDate);
        } catch (ParseException e) {
            forward("/app/error.jsp?currentTripID="+trip.getTripID()+"&error="+e.getMessage(),request,response);
        }
        newExpense.setCategory(ExpenseCategory.valueOf(request.getParameter("expenseCategory")));
        newExpense.setNotes(request.getParameter("expenseNotes"));
        try {
            newExpense.saveObj();
        } catch (SQLException e) {
            e.printStackTrace();
            forward("/app/error.jsp?currentTripID="+trip.getTripID()+"&error="+e.getMessage(),request,response);
        }
        return newExpense;
    }

    private void addUsersToExpense(HttpServletRequest request, Expense newExpense) throws SQLException {
        User collector = User.getUser(Integer.parseInt(request.getParameter("expenseCollector")));
        UserExpense collectorUserExpense = new UserExpense();
        collectorUserExpense.setExpense(newExpense);
        collectorUserExpense.setIsCollector(true);
        collectorUserExpense.setUser(collector);
        collectorUserExpense.saveObj();

        for (String debtorID : request.getParameterValues(("expenseDebtors"))) {
            User debtor = User.getUser(Integer.parseInt(debtorID));
            UserExpense debtorUserExpense = new UserExpense();
            debtorUserExpense.setExpense(newExpense);
            debtorUserExpense.setIsCollector(false);
            debtorUserExpense.setUser(debtor);
            debtorUserExpense.saveObj();
        }
    }

    protected void forward(String url, HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        try {
            dispatcher.forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(TripSplitController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TripSplitController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "TripSplit helps users split the costs incurred on a road trip.";
    }
}
