/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tripsplit.api;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author brian
 */
@DatabaseTable(tableName = "TS_UserTripJoin")
public class UserTrip extends DaoCommon implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="static variables">
	private static final long serialVersionUID = -4435288787197114231L;
	// column names    
    public final static String COLUMN_USER_ID = "user_id";
    public final static String COLUMN_TRIP_ID = "trip_id";

            // </editor-fold>
    
    @SuppressWarnings("unchecked")
	public UserTrip getUserTrip(int userId,int tripId) throws SQLException{
        UserTrip tempUserTrip = new UserTrip();
        //
        QueryBuilder<UserTrip, String> queryBuilder = tempUserTrip.getDao().queryBuilder();
        Where<UserTrip, String> where = queryBuilder.where();
        where.eq(UserTrip.COLUMN_USER_ID, userId);
        where.and();
        where.eq(UserTrip.COLUMN_TRIP_ID, tripId);
        PreparedQuery<UserTrip> preparedQuery = queryBuilder.prepare();
        List<UserTrip> tempUTList = tempUserTrip.getDao().query(preparedQuery);
        if(tempUTList!=null && 0<tempUTList.size()){
            tempUserTrip = (UserTrip) tempUTList.get(0);
            return tempUserTrip;
        }
        else return null;
    }
    
    // <editor-fold defaultstate="collapsed" desc="Variables and database fields">
    @DatabaseField(generatedId = true)
    private int userTripID;

    @DatabaseField(foreign = true, index = true, columnName = COLUMN_USER_ID)
    private User user;

    @DatabaseField(foreign = true, index = true, columnName = COLUMN_TRIP_ID)
    private Trip trip;

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="constructor">

    public UserTrip() {

    }

    public UserTrip(User user, Trip trip) {
        this.user = user;
        this.trip = trip;
    }
        // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Get and Set for variables">
    public int getUserTripID() {
        return userTripID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    @Override
    public String getClassName() {
        return UserTrip.class.getSimpleName();
    }

// </editor-fold>
}
