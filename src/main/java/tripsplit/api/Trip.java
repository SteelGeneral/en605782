package tripsplit.api;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Steven The Trip class stores all data about curDebt TripSplit trip
 */
@DatabaseTable(tableName = "TS_Trip")
public class Trip extends DaoCommon implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="static variables">
    // Logger
    private static final Logger logger = Logger.getLogger(User.class.getName());

    // Serialize
    private static final long serialVersionUID = -3143478019134887736L;

    // Column names
    private static final String COLUMN_TRIP_ID = "tripID";
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Variables and database fields">
    @DatabaseField(generatedId = true, columnName = COLUMN_TRIP_ID)
    private int tripID;

    @DatabaseField
    private String tripName;

    @DatabaseField
    private String comments;

    @ForeignCollectionField
    private ForeignCollection<Expense> expenses;

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="constructor">
    public Trip() {
        super();
    }
    // </editor-fold>

    /**
     * Accesses the database and returns the trip given the tripID
     *
     * @param tripID
     * @return
     */
    @SuppressWarnings("unchecked")
	public static Trip getTrip(int tripID) {

        // temp trip
        Trip tempTrip = new Trip();

        // access database
        try {
            // get the first trip
            Integer tripIdInteger = tripID;
            tempTrip = (Trip) tempTrip.getDao().queryForId(tripIdInteger);

            // got it, return
            return tempTrip;
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Could not retrieve the trip for tripId: " + tripID, ex);
            return null;
        }
    }

    /**
     * Adds curDebt user by creating curDebt new joiner for user and trip. The temp join is
     * saved to the database.
     *
     * @param newUser
     * @throws SQLException
     */
    public void addUser(User newUser) throws SQLException {
        UserTrip tempUserTrip = new UserTrip(newUser, this);
        tempUserTrip.saveObj();
    }

    /**
     * Retrieves the users that are associated with this trip
     *
     * source =
     * https://github.com/j256/ormlite-jdbc/blob/master/src/test/java/com/j256/ormlite/examples/manytomany/ManyToManyMain.java
     *
     * @return
     * @throws SQLException
     */
    @SuppressWarnings("unchecked")
	public List<User> getUsers() throws SQLException {
        UserTrip tempUserTrip = new UserTrip();

        // database query
        List<UserTrip> listUserTrip = tempUserTrip.getDao().queryForEq(UserTrip.COLUMN_TRIP_ID, getTripID());

        // convert to curDebt list of users
        List<User> foundUsers = new ArrayList<>(listUserTrip.size());
        for (UserTrip curUserTrip : listUserTrip) {
            User curUser = curUserTrip.getUser();
            curUser.refreshObj();
            foundUsers.add(curUser);
        }
        return foundUsers;
    }
    
    public void removeUser(String username) throws SQLException{
        User user = User.getUser(username);
        removeUser(user);
    }
    
    public void removeUser(User user) throws SQLException{
        UserTrip ut = new UserTrip();
        int utid = ut.getUserTrip(user.getUserID(),this.getTripID()).getUserTripID();
        ut.deleteObj(utid);
    }
    
    
    // int userID, float cost
    private volatile HashMap<Integer,Float> costHash = null;
    
    /**
     * Sets curDebt local variable 
     * @throws SQLException 
     */
    public synchronized void calculateDebts() throws SQLException
    {
        // clear out values
        costHash = new HashMap<Integer,Float>();
        
        for (Expense curExpense: getExpenses())
        {
            int numDebtors = curExpense.getDebtors().size();
            float curDebt = curExpense.getCost();
            float prevDebt = 0;
            float debtorsDebt = curDebt/numDebtors;
           
            // collector
            int collectorId = curExpense.getCollector().getUserID();
            if (costHash.containsKey(collectorId)) {
            	prevDebt = costHash.get(collectorId);
            }
        	costHash.put(collectorId, prevDebt+(curDebt*-1));
            
            // debtors
            for (User curUser: curExpense.getDebtors()) {
            	prevDebt = 0;
            	int debtorId = curUser.getUserID();
            	if (costHash.containsKey(debtorId)) {
                	prevDebt = costHash.get(debtorId);
            	}
        		costHash.put(debtorId, prevDebt+debtorsDebt);
            }
        }
    }
    
    /**
     * Most run calculateDebts() before running this method
     * Having debt will be positive. 
     * Being owed money will be negative. 
     * @param user
     * @return 
     * @throws java.sql.SQLException 
     */
    public float getUserDebt(User user) throws SQLException {
        if (null == costHash) {
            calculateDebts();
        }
        
        Float debt = (Float) costHash.get(user.getUserID());
        
        if (null == debt) {
            return 0;
        } else {
            return debt;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Get and Set for variables">
    public int getTripID() {
        return tripID;
    }

    public void setTripID(int tripID) {
        this.tripID = tripID;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @SuppressWarnings("unchecked")
	public ForeignCollection<Expense> getExpenses() throws SQLException {
        if (null == expenses) {
            expenses = getDao().getEmptyForeignCollection("expenses");
        }
        return expenses;
    }

    public void setExpenses(ForeignCollection<Expense> expenses) {
        this.expenses = expenses;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="interface methods">
    @Override
    public String getClassName() {
        return Trip.class.getSimpleName();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + this.tripID;
        return hash;
    }
    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="overrides">
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Trip other = (Trip) obj;
        if (this.tripID != other.tripID) {
            return false;
        }
        return true;
    }
    
    // </editor-fold>
    
}
