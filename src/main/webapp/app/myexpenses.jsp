<%@page import="
	tripsplit.api.User,
	tripsplit.api.Trip,
	tripsplit.api.Expense,
	tripsplit.api.ExpenseCategory"
%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>TripSplit - My Expenses</title>
		<link rel="stylesheet" href="../css/style.css">
        <jsp:useBean id="currentUser" scope="session" class="tripsplit.api.User"/>		
	</head>
	<body>
		<%@include file="navbar.jsp"%>
        <section id="trip-banner">My Expenses</section>        		
		<section id="left">
			<table id="table-a">
			<caption>My Debts</caption>
	        <tr>
	        	<th>Trip</th>
	            <th>Expense Name</th>
	            <th>Cost</th>
	            <th>Date</th>
	            <th>Category</th>
	            <th>Collector</th>
	            <th>Debtors</th>
	            <th>Notes</th>
	        </tr>
		        <% 
		        	List<Trip> currentTrips = (List<Trip>) request.getSession().getAttribute("currentTrips");
		        	String debts = "";
		        	String owed = "";
					String tripTotalDebt = "";
					float totalDebt = 0;
		        	if (currentTrips != null) {
			        	for(Trip t : currentTrips) { 
			        		if (t.getTripName() != null ) {
				        		for(Expense e : t.getExpenses()){
				        			for (User d : e.getDebtors()){
				        				if(currentUser.getUserID() == d.getUserID()){
				        					debts+="<tr>"
					                            +"<td>"+t.getTripName()+"</td>"
					                            +"<td>"+e.getTitle()+"</td>"
					                            +"<td>"+formatter.format(e.getCost())+"</td>"
					                            +"<td>"+e.getDate()+"</td>"
					                            +"<td>"+e.getCategory()+"</td>"
					                            +"<td>"+e.getCollector().getUserName()+"</td>";
				                            String debtors = "";
				                            for (User debtor : e.getDebtors()){
				                            	debtors+=debtor.getUserName()+"<br>";
				                            }
				                            debts+="<td>"+debtors+"</td>"
					                            +"<td>"+e.getNotes()+"</td>"
					                            +"</tr>";
				        				}
				        			}
				        			if (currentUser.getUserID() == e.getCollector().getUserID()) {
			        					owed+="<tr>"+
				                            "<td>"+t.getTripName()+"</td>"
				                            +"<td>"+e.getTitle()+"</td>"
				                            +"<td>"+formatter.format(e.getCost())+"</td>"
				                            +"<td>"+e.getDate()+"</td>"
				                            +"<td>"+e.getCategory()+"</td>"
				                            +"<td>"+e.getCollector().getUserName()+"</td>";
			                            String debtors = "";
			                            for (User debtor : e.getDebtors()){
			                            	debtors+=debtor.getUserName()+"<br>";
			                            }
			                            owed+="<td>"+debtors+"</td>"
				                            +"<td>"+e.getNotes()+"</td>"
				                            +"</tr>";        				
				        			}
				        		}
				        		float tripDebt = t.getUserDebt(currentUser); 
				        		totalDebt += tripDebt;
			            		if (tripDebt == 0) {
					        		tripTotalDebt+="<tr><td>"+t.getTripName()+"</td><td>"+formatter.format(tripDebt)+"</td></tr>";
			            		} else if (tripDebt < 0) {
					        		tripTotalDebt+="<tr><td>"+t.getTripName()+"</td><td style='color:green'>"+formatter.format(tripDebt)+"</td></tr>";
			            		} else {
					        		tripTotalDebt+="<tr><td>"+t.getTripName()+"</td><td style='color:red'>"+formatter.format(tripDebt)+"</td></tr>";		            			
			            		}
				        	}
			        	}
			        }
		        	out.print(debts);
		        %>	        
	        </table>
			<table id="table-a">
				<caption>My Collections</caption>
		        <tr>
		        	<th>Trip</th>
		            <th>Expense Name</th>
		            <th>Cost</th>
		            <th>Date</th>
		            <th>Category</th>
		            <th>Collector</th>
		            <th>Debtors</th>
		            <th>Notes</th>
		        </tr>
		        <% out.print(owed); %>
	    	</table>
    	</section>
    	<section id="right">
	    	<table id="table-a">
	    		<caption>Total Debts</caption>
	    		<tr>
	    			<th>Trip</th>
	    			<th>Debt</th>
	    		</tr>
	    		<% out.print(tripTotalDebt);%>
	    		<tr>
	    			<td><b>Total</b></td>
		    		<%
		           		if (totalDebt == 0) {
			            	out.write("<td><b>"+formatter.format(totalDebt)+"</b></td>");			            			           			
		           		} else if (totalDebt < 0) {
			            	out.write("<td style='color:green'><b>"+formatter.format(totalDebt)+"</b></td>");			            			           			           			
		           		} else {
			            	out.write("<td style='color:red'><b>"+formatter.format(totalDebt)+"</b></td>");			            			           			           			
		           		}
		    		 %>
	    		</tr>
	    	</table>
    	</section>
	</body>
</html>