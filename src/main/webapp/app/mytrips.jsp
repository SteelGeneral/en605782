<%@page import="tripsplit.api.Trip"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>TripSplit - My Trips</title>
        <link rel="stylesheet" href="../css/style.css">
        <!-- Source for Javascript: http://imar.spaanjaars.com/312/how-do-i-make-a-full-table-row-clickable -->
        <script type="text/javascript">
            function DoNav(theUrl)
            {
                document.location.href = theUrl;
            }
        </script>
    </head>
    <body>
        <%@include file="navbar.jsp"%>
        <section id="trip-banner">My Trips</section>        
        <table id="table-a">
            <caption>Trips</caption>
            <tr>
                <th>Name</th>
                <th>Comments</th>
            </tr>
            <%  List<Trip> currentTrips = (List<Trip>) request.getSession().getAttribute("currentTrips");
                if (currentTrips != null) {
                    for (Trip trip : currentTrips) {
                        if (null != trip.getTripName()) {
            %>
				            <tr onclick="DoNav('trip.jsp?currentTripID=<%=trip.getTripID()%>');"> 
				                <td style="color:blue; text-decoration:underline"><%=trip.getTripName()%></td>
				                <td><%=trip.getComments()%></td>
				            </tr>
            <%
                        }
                    }
                }
            %>
        </table>
    </body>
</html>