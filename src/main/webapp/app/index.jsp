<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>TripSplit - Home</title>
		<link rel="stylesheet" href="../css/style.css">
	</head>
	<body>
		<%@include file="navbar.jsp"%>
        <h2>About TripSplit</h2>
        <p>
            Without your friends, a road trip is just a long drive. But keeping track of who paid for what can be tricky. Convincing that one buddy he owes you money can be even trickier - you know the one. That's where TripSplit comes in. With TripSplit, you can keep track of each trip, and each expense. It's easy:
        </p>

        <ol>
            <li>Create your trip</li>
            <li>Add your friends</li>
            <li>Add expenses</li>
            <li>TripSplit does the rest!</li>
        </ol>
        <p><a href='create.jsp'>Get Started Now!</a></p>
	</body>
</html>