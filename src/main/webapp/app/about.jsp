<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <title>TripSplit - About</title>
	    <link rel="stylesheet" href="../css/style.css">
	</head>
	<body>
		<%@include file="navbar.jsp"%>
        <section id="trip-banner">About Us</section>        			
		<p><span style="text-decoration:underline">About TripSplit</span></p>
		<p>TripSplit is a web application that was made to enable multiple users to sign on and collaborate in planning a trip. Road trips will be specifically targeted as a niche user group. TripSplit will act as a cost calculator, allowing users to distribute costs during the planning period. Each user will have a stored login, and each trip will also have a unique ID. Users will be able to access their trip(s) across multiple sessions. </p>
	    <p><span style="text-decoration:underline">About the Developers</span></p>
	    Steven Pritchett is a Software Engineer with a primary background in Python and Java (including using Spring, Maven, and RESTful webservices) as well as a background in C++, C#, Android, and Objective-C.
	    <br>
	    Kelly Hatfield is a Software Engineer with a background in HTML/Javascript and PHP web development, as well as Java and Android programming.
	    <br>
		Seth Marple is a Software Engineer with a background in Java and C# web development, using technologies including SharePoint, Maven, Spring MVC, Vaadin UI, JavaScript, JQuery, and Hibernate.
		<br> 
		Brian Goodacre is a Software Engineering with a background in Java, C#, and C. He has developed across the software stack using technologies like AngularJS, Hibernate, SVN, GIT, and others.
	</body>
</html>