<%@page import="
        tripsplit.api.User,
        tripsplit.api.Trip,
        tripsplit.api.Expense,
        tripsplit.api.ExpenseCategory,	
        java.util.List,
        com.j256.ormlite.dao.ForeignCollection"
        %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>TripSplit - Trip</title>
        <link rel="stylesheet" href="../css/style.css">
        <jsp:useBean id="currentUser" scope="session" class="tripsplit.api.User"/>
        <style>
            table{margin-left:20%;margin-top:50px;}
            td,th{padding:10px;}
            .error{color:red;}
        </style>
        <script>
            function confirmRemove(){
                return confirm("Are you sure you want to delete this user? Deleting a user will not change the expenses owed.");
            }
            
            function validate() {
                document.getElementById("expenseTitleError").innerHTML = "";
                document.getElementById("expenseCostError").innerHTML = "";
                document.getElementById("expenseDateError").innerHTML = "";
                document.getElementById("expenseCategoryError").innerHTML = "";
                document.getElementById("expenseCollectorError").innerHTML = "";
                document.getElementById("expenseDebtorsError").innerHTML = "";
                document.getElementById("expenseNotesError").innerHTML = "";
                var expenseTitle = document.forms["expenseForm"]["expenseTitle"].value;
                var expenseCost = document.forms["expenseForm"]["expenseCost"].value;
                var expenseDate = document.forms["expenseForm"]["expenseDate"].value;
                var expenseCategory = document.forms["expenseForm"]["expenseCategory"].value;
                var expenseCollector = document.forms["expenseForm"]["expenseCollector"].value;
                var expenseDebtors = document.forms["expenseForm"]["expenseDebtors"].value;
                var expenseNotes = document.forms["expenseForm"]["expenseNotes"].value;
                valid = true;
                if (expenseTitle == null || expenseTitle == "") {
                    document.getElementById("expenseTitleError").innerHTML = "Required field, please add a value";
                    valid = false;
                }
                if (expenseCost == null || expenseCost == "") {
                    document.getElementById("expenseCostError").innerHTML = "Required field, please add a value";
                    valid = false;
                }
                if (isNaN(Number(expenseCost))) {
                    document.getElementById("expenseCostError").innerHTML = "Cost must be a number";
                    valid = false;
                }
                if (expenseDate == null || expenseDate == "") {
                    document.getElementById("expenseDateError").innerHTML = "Required field, please add a value";
                    valid = false;
                }
                if (!/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(expenseDate)) {
                    document.getElementById("expenseDateError").innerHTML = "Date must be in the format YYYY-MM-DD";
                    valid = false;
                }
                if (expenseCategory == null || expenseCategory == "") {
                    document.getElementById("expenseCategoryError").innerHTML = "Required field, please add a value";
                    valid = false;
                }
                if (expenseCollector == null || expenseCollector == "") {
                    document.getElementById("expenseCollectorError").innerHTML = "Required field, please add a value";
                    valid = false;
                }
                if (expenseDebtors == null || expenseDebtors == "") {
                    document.getElementById("expenseDebtorsError").innerHTML = "Required field, please add a value";
                    valid = false;
                }
                return valid;
            }
        </script>    
    </head>
    <body>
        <%@include file="navbar.jsp"%>        
        <%  
            // variables in the try/catch
            List<User> currentUsers = null;
            Trip currentTrip  = null;
            int currentTripID = -1;
            ForeignCollection<Expense> currentExpenses = null;
            
            try {
                currentTripID = Integer.parseInt(request.getParameter("currentTripID"));
                currentTrip = new Trip().getTrip(currentTripID);
                session.setAttribute("trip", currentTrip);
                currentUsers = currentTrip.getUsers();
                currentExpenses = currentTrip.getExpenses();
            } catch (Exception e) {
                session.setAttribute("errorMsg", "Could not open that trip.");
                getServletContext().getRequestDispatcher("/app/error.jsp").forward(request, response);
            }
            
            // Error if the user is not part of this trip
            if (! currentUsers.contains(currentUser)) {
                session.setAttribute("errorMsg", "You are not a member of this trip.");
                getServletContext().getRequestDispatcher("/app/error.jsp").forward(request, response);
            }
        %>
       	<section id="trip-banner"><%=currentTrip.getTripName() %></section>
        <section id="left">
	        <table id="table-a">
	            <caption>Trip Expenses</caption>
	            <tr>
	                <th>Expense Name</th>
	                <th>Cost</th>
	                <th>Date</th>
	                <th>Category</th>
	                <th>Collector</th>
	                <th>Debtors</th>
	                <th>Notes</th>
	                <th>Action</th>
	            </tr>
	            <%
	                if (currentTrip != null) {
	                    if (currentExpenses != null) {
	                        for (Expense exp : currentExpenses) {
	            %>
					            <tr>
					            	<td><%=exp.getTitle() %></td>
					            	<td><%=formatter.format(exp.getCost()) %></td>
					            	<td><%=exp.getDate() %></td>
					            	<td><%=exp.getCategory() %></td>
					            	<td><%=exp.getCollector().getUserName() %></td>
						            <%
						            	String debtors = "";
						                for (User debtor : exp.getDebtors()) {
						                	debtors += debtor.getUserName() + "<br>";
						                }
						            %>
	            					<td><%=debtors %></td>
	            					<td><%=exp.getNotes() %></td>
	            					<td>
	            						<form method='get' action='TripSplitController'>
	            							<input type='hidden' id='removeExpense' name='removeExpense' value='<%=exp.getExpenseID()%>'/>
	            							<input type='submit' value='Remove'/>
	            						</form>
	            					</td>
	            				</tr>
	            <%
	                        }
	                    }
	                }
	            %>                   
	            <form method='get' action='TripSplitController' onsubmit='return validate()' id='expenseForm'>
	                <tr>
	                    <td>
	                    	<p class="error" id="expenseTitleError"></p>
	                    	<label>Name: </label>
	                    	<input type="text" name="expenseTitle" id="expenseTitle"/>
	                    </td>
	                    <td>
	                    	<p class="error" id="expenseCostError"></p>
	                    	<label>Cost: $</label>
	                    	<input type="text" name="expenseCost" id="expenseCost"/>
	                    </td>
	                    <td>
	                    	<p class="error" id="expenseDateError"></p>
	                    	<label>Date (YYYY-MM-DD): </label>
	                    	<input type="date" name="expenseDate" id="expenseDate"/>
	                    </td>
	                    <td>
	                    	<p class="error" id="expenseCategoryError" ></p>
	                        <label>Category: </label>
	                        <select name="expenseCategory" id="expenseCategory">
	                            <% for (ExpenseCategory item : ExpenseCategory.values()) {%>
	                            	<option value="<%=item%>"><%=item%></option>
	                            <% } %>
	                        </select>
	                    </td>
	                    <td>
	                    	<p class="error" id="expenseCollectorError"></p>
	                        <label>Collector: </label>
	                        <select name="expenseCollector" id="expenseCollector">
	                            <% for (User user : currentUsers) {%>
	                            	<option value="<%=user.getUserID()%>"><%=user.getUserName()%></option>
	                            <% } %>
	                        </select>
	                    </td>
	                    <td>
	                    	<p class="error" id="expenseDebtorsError"></p>
	                        <label>Debtors: </label>
	                        <select multiple id="expenseDebtors" name="expenseDebtors" size="5">
	                            <% for (User user : currentUsers) {%>
	                            	<option value="<%=user.getUserID()%>" ><%=user.getUserName()%></option>
	                            <% } %>
	                        </select>	                
	                    </td>
	                    <td>
	                    	<p class="error" id="expenseNotesError"></p>
	                    	<label>Notes: </label>
	                    	<input type="text" name="expenseNotes" id="expenseNotes"/>
	                    </td>
	                    <td>
	                        <input type='hidden' id='addExpense' name='addExpense' value='true' />
	                        <input type="submit" value="Create" onclick="return validate()"/>
	                    </td>
	                </tr>
		    </form>                
	    </table> 
    </section>
   	<section id="right">    	
	    <table id="table-a">
	        <caption>Manage Members</caption>
	        <tr>
	            <th>Name</th>
	            <th>Action</th>
	        </tr>
	            <%
	                if (currentTrip != null) {
	                    if (currentUsers != null) {
	                        for (User u : currentUsers) {
	 			%>
	 						 <tr>
	                            <td><%=u.getUserName() %></td>
	                            <td>
	                                <form method='get' action='TripSplitController' onsubmit="return confirmRemove()">
	                            		<input type='hidden' id='removeMember' name='removeMember' value='<%=u.getUserName()%>'/>
	                            		<input type='submit' value='Remove'/>
	                            	</form>
	                            </td>
	                        </tr>
	                    
	            <%
	                        }
	                    }
	                }
	            %>
		        <form method='get' action='TripSplitController' id='addMembersForm'>
		            <tr>
		                <td>
		                    <select multiple id="members" name="members" size="5">
		                        <% 
	                                List<User> curTripUsers = currentTrip.getUsers();
	                                for (User user : new User().getAllUsers()) {
	                                    if (!curTripUsers.contains(user))
	                                    {%>
		                        	<option value="<%=user.getUserName()%>"><%=user.getUserName()%></option>
		                        <% }}%>
		                    </select>
		                </td>
		                <td>
		                    <input type='hidden' id='addMembers' name='addMembers' value='true' />
		                    <input type="submit" value="Add To Trip" id="addMembers"/>
		                </td>
		            </tr>
		        </form>
	    </table> 
    </section> 
    <section id="left">
	    <table id="table-a">
	            <caption>Total Debt</caption>
	            <tr>
	                <th>Name</th>
	                <th>Debt</th>
	            </tr>
	            <% for (User user : currentUsers) { %>
	            	<tr>
		            	<td><%=user.getUserName()%></td>
		            	<%
		            		float userDebt = currentTrip.getUserDebt(user);
		            		if (userDebt == 0) {
		    	            	out.write("<td>"+formatter.format(userDebt)+"</td>");			            			
		            		} else if (userDebt < 0) {
		    	            	out.write("<td style='color:green'>"+formatter.format(userDebt)+"</td>");	
		            		} else {
		    	            	out.write("<td style='color:red'>"+formatter.format(userDebt)+"</td>");	
		            		}
		            	%>
		            </tr>
	            <% } %>  
	   	</table>
   	</section>  
</body>
</html>
