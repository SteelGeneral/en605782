<%@page import="tripsplit.api.User"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>TripSplit - Create</title>
        <link rel="stylesheet" href="../css/style.css">
        <script>
            function validate(){
                document.getElementById("tripNameError").innerHTML = "";
                var name = document.forms["tripForm"]["tripName"].value;
                if (name == null || name == "") {
                    document.getElementById("tripNameError").innerHTML = "Required field, please add a value";
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>   
        <%@include file="navbar.jsp"%>
        <section id="trip-banner">Create Trip</section>                
        <form method="get" action="TripSplitController" id="tripForm" onsubmit="return validate()">
        	<table id="table-a">
	        	<caption>New Trip</caption>
	        	<tr>
	        		<th>Trip Name</th>
	        		<th>Comments</th>
	        		<th>Users</th>
	        		<th>Action</th>
	        	</tr>
	        	<tr>        		
		            <td>
		            	<label for="tripName">Trip Name: </label>
		            	<input type="text" name="tripName" id="tripName"/>
		            </td>
		            <td>
		            	<label for="comments">Comments: </label>
		            	<input type="text" name="comments" id="comments" />
		            </td>
		            <td>
			            <select multiple id="members" name="members" size="5">
			                <% for (User user : new User().getAllUsers()) {%>
			                	<option value="<%=user.getUserName()%>"><%=user.getUserName()%></option>
			                <% }%>
			            </select>
		            </td>
		            <td>
		            	<input type="hidden" name="newTrip" id="newTrip" value="newTrip"/>
		            	<input type="submit" value="Create Trip" onclick="return validate()"/>
		            </td>
		    	</tr>
            </table>
        </form>
    </body>
</html>